# lexer-with-flex-and-lisp
This reporsitory contains 2 different implementation of home-brewed, full flavored language G++ (rules are provided in the pdf file). 

The lexer-using-flex folder contains implementation using flex tool.
The lexer-using-lisp folder contains implementation using lisp language from stratch.


For lexer-using-flex;

flex gpplexer.l

gcc lex.yy.c

./a.out 

or 

./a.out inputfile


For lexer-using-lisp;

clisp gpplexer.lisp

or

clisp gpplexer.lisp inputfile
